REM BACKUP SCRIPT EXAMPLE
REM *********************************

set start=%TIME:~0,8%
del mirror.db

REM example for backup folder..1
copy  /y mirror-folder-1.db  mirror.db
powershell "python Mirror-Zip-Backup.py \"S:\SRC1\" \"T:\TARGET1\" \"---password---\" | tee folder1.log"
copy  /y mirror.db  mirror-folder-1.db
copy  /y mirror.db  "T:\mirrordb_backup\mirror-folder-1.db"
del mirror.db

REM example for backup folder..2
copy  /y mirror-folder-2.db  mirror.db
powershell "python Mirror-Zip-Backup.py \"S:\SRC2\" \"T:\TARGET2\" \"---password---\" | tee folder2.log"
copy  /y mirror.db  mirror-folder-2.db
copy  /y mirror.db  "T:\mirrordb_backup\mirror-folder-2.db"
del mirror.db

REM example rclone sync to cloud drive
rclone sync -P T:\TARGET1 cloud_drive:/PATH/1
rclone sync -P T:\TARGET2 cloud_drive:/PATH/2

set stop=%TIME:~0,8%
echo start = %start%
echo stop = %stop%
pause

