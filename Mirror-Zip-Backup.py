# project: MirrorZipBackup
# objective: mirror files backup with 7zip compression
# by: tkvr
# last date: 3/5/2022
#
# objective:
#   - backup and compress all modified files (one by one)
#   - design for 'OneDrive' (automatic resync with small piece update)
#
# bug fix:
    # - can't copy file if without extension
    # - sometime error at step2 (while remove folder)
#    
# add
    # save only path (no include drive)
#
# helper from class 'pathlib.WindowsPath'
    # ex: get file size 
    # results[1].stat().st_size

    # ex: get last modified
    # datetime.fromtimestamp( results[1].stat().st_mtime )

    # ex: get full path string 
    # str( results[1] )

#from os.path import isfile, isdir, join
from msilib.schema import RemoveFile
from pathlib import Path
from datetime import datetime
import os
import sys
import glob
import pickle
import shutil
import subprocess


def set_default( msg:str, default_txt:str ) -> str:
    inp = str( input( msg + ": ") )
    return default_txt if not inp else inp

def debug( msg:object ):
    print( msg if DEBUG else "" )

# list all files only (no folder) include subfolder
def list_all_files( srcPath:str ) -> list:
    return list(Path( srcPath ).rglob( "*" ))
    #return list(Path( srcPath ).rglob( "*.*" )) # bug

def get_all_subfolders( srcpath:str ) -> list:
    return glob.glob(f'{srcpath}/*/**/', recursive = True)

def file_exist( filename:str ) -> bool:
    return os.path.exists( filename )

def create_folder( fname:str ):
    if not os.path.exists( fname ):
        os.makedirs( fname )

# create folder and subfolder like a mirror
def mirrordir( srcpath:str, tarpath:str ):
    subfolders = get_all_subfolders( srcpath )
    print("create folder..")
    for folder in subfolders:
        fname = folder.replace( srcpath, tarpath)
        create_folder( fname )
        debug("  => %s" % ( fname ) )

# complete !
def dirs_not_exist(srcpath:str, tarpath:str ) -> list:
    src_folders = get_all_subfolders( srcpath )
    src_folders = set( [ fix.replace( srcpath, tarpath ) for fix in src_folders ] )
    tar_folders = set( get_all_subfolders( tarpath ) )
    notexist = list( tar_folders.union( src_folders ) - src_folders )
    notexist.sort(reverse=True)
    #debug(notexist)
    return notexist

# complete -_-
def files_not_exist(srcpath:str, tarpath:str ) -> list:
    all_files = list_all_files( srcpath ) # -> class 'pathlib.WindowsPath'
    src_files = set( [ str(fix).replace( srcpath, tarpath )+".7z" for fix in all_files ] )
    all_files = list_all_files( tarpath )
    tar_files = set( [ str(path) for path in all_files ] )
    notexist = list( tar_files.union( src_files ) - src_files )
    notexist.sort(reverse=True)
    #debug(notexist)
    return notexist

# extract full path and get only folder name
def get_foldername( fullpath:str ) -> str:
    return os.path.dirname( os.path.abspath( fullpath ))

# # extract full path and get only filename (include extension)
def get_filename( fullpath:str ) -> str:
    return os.path.split( os.path.abspath( fullpath ))[1] # 0 = folder, 1 = filename and extension

def copy_file( src:str, tar:str ):
    shutil.copyfile(src, tar)
    debug("copy file %s -> %s " % (src, tar) )

# 7zip backup file
def backup_file( src:str, tar:str ):
    # 7zip compression (** 7z use "name.7z" for "first" parameter follow by files who want to compress)    
    args = SEVENZIP_PATH + (SEVENZIP_PARAM % ( tar, src, "--password--" ))
    debug("7z cmd = " + args)
    args = SEVENZIP_PATH + (SEVENZIP_PARAM % ( tar, src, SEVENZIP_PASS ))
    FNULL = open(os.devnull, 'w')    #use this if you want to suppress output to stdout from the subprocess   
    subprocess.call(args, stdout=FNULL, stderr=FNULL, shell=False)
    
def load_list( filename:str ) -> list:
    with open( filename, "rb") as fp:
        result = pickle.load(fp)
    return result

def save_list( savelist:list, filename:str ):
    with open( filename, "wb") as fp:
        pickle.dump(savelist, fp)


# ===================================================
# Global variable
# ===================================================
# print ("currdir : " + os.path.dirname(os.path.realpath(__file__)))
DEBUG = True
MIRROR_DB = "mirror.db"

# ============================================================================

# type 1: get argument from commandline
if len(sys.argv) < 3:
    sys.exit("error: require 3 arguments 'SRC DES PASS'")
SRCPATH = sys.argv[1].rstrip('\\')
TARPATH = sys.argv[2].rstrip('\\')
SEVENZIP_PASS = sys.argv[3]
SEVENZIP_PATH = r'C:\Program Files\7-Zip\7z.exe'


'''
# type 2: input from console
SRCPATH = set_default( "source path", r'X:\PrivatePersonal' ).rstrip('\\')
TARPATH = set_default( "target path", r'Z:\PrivatePersonal' ).rstrip('\\')
SEVENZIP_PASS = set_default( "7zip password", "" )
SEVENZIP_PATH = set_default( "7zip path", r'C:\Program Files\7-Zip\7z.exe' )
'''
# ============================================================================

# insert '-p"SECRET"' parameter  if password not blank
SEVENZIP_PASS = "-p\"%s\"" % SEVENZIP_PASS if SEVENZIP_PASS else ""
SEVENZIP_PARAM = " a -t7z \"%s\" \"%s\" %s"
if not file_exist( SEVENZIP_PATH ):
    sys.exit("error: compression not found !")
    

# ===================================================
# main
# ===================================================
#0. load old data
now_current_files = []
old_recorded_files = []
if file_exist( MIRROR_DB ):
    old_recorded_files = load_list( MIRROR_DB )
else:
    print("mirror.db not found..\n")

# create target folder like a mirror
mirrordir( SRCPATH, TARPATH )

# step 1
print("\nStep 1# backup files..")
#1. list all file from src dir
file_counter = 0
backup_file_counter = 0
all_files = list_all_files( SRCPATH )
print( "%s files and folder found" % len(all_files) )

#2. start backup
for file in all_files:
    file_counter += 1
    src_filename = str(file) # full path
    if os.path.isdir( src_filename ):
        print( "(%s/%s)" % (file_counter, len(all_files)) )
        debug( "src: %s => is directory" %  src_filename )
        continue

    target_filename = src_filename.replace( SRCPATH , TARPATH )  + ".7z" 
    print( "(%s/%s)" % (file_counter, len(all_files)) )
    debug( "src: %s\ntar: %s" % ( src_filename, target_filename) )

    # status structure [ filename, size, last modifieds ]
    file_status = [ src_filename.replace( SRCPATH , '' ), file.stat().st_size, datetime.fromtimestamp(file.stat().st_mtime) ]
    if file_exist( target_filename ) and (file_status in old_recorded_files):
        pass
    else:
        # if 7z not exist   then create a new one
        print("%s creating.." % target_filename)
        backup_file( src_filename, target_filename )
        backup_file_counter += 1
        print("..complete\n")

    # save record to list 
    now_current_files.append( file_status )

# save list to file
save_list( now_current_files, MIRROR_DB )

# step 2 delete all difference files and folder
print("\nStep 2# remove temp files and folder..")
# delete file first
delete_file_counter = 0
fn_exist = files_not_exist( SRCPATH, TARPATH )
for file in fn_exist:
    if os.path.isfile( file ):
        os.remove( file )
        delete_file_counter += 1
        print( "file: %s => deleted!" % file)

delete_folder_counter = 0
ds_exist = dirs_not_exist( SRCPATH, TARPATH )
for dir in ds_exist:
    if os.path.isdir( dir ):
        os.rmdir( dir )
        delete_folder_counter += 1
        print( "folder: %s => deleted!" % dir)

print("\n%s Total files backup.." % backup_file_counter)
print("%s Total files delete.." % delete_file_counter)
print("%s Total folder delete.." % delete_folder_counter)

print("\nend.")
